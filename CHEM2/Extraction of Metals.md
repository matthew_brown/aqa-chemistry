# Extraction Of Metals
## **Sulfide** Ores (examples: Zinc Sulfide, Lead Sulfide)
* Roast with oxygen
  * Produces SO2 (causes acid rain)

## **Oxide** and **Carbonate** Ores (examples Fe2O3, MnO2, CuCO3)
* Reduce with carbon or carbon monoxide
  * Produces CO3
  * Cannot be used with Titanium (forms Titanium Carbide)
* Temp requirements
    * Fe - 700 &deg;C
    * Mn - 1200 &deg;C

*
